package com.example.gt303_12.breakoutgame;

import android.graphics.RectF;

public class Brick {
    private RectF rect;
    private boolean isVisible;

    public Brick(int row, int width, int column,  int height){
        isVisible = true;

        int padding = 1;

        rect = new RectF(column * width + padding,
                row * height + padding,
                column * width + width - padding,
                row * height + height - padding);
    }

    public RectF getRect(){
        return this.rect;
    }

    public void setInvisible(){
        isVisible = false;
    }

    public boolean getVisibility(){
        return isVisible;
    }
}

