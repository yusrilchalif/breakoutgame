package com.example.gt303_12.breakoutgame;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class BreakoutGame extends Activity {

    BreakoutView breakoutView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        breakoutView = new BreakoutView(this);
        setContentView(breakoutView);
    }

    class BreakoutView extends SurfaceView implements Runnable{
        Thread gameThread = null;
        SurfaceHolder ourHolder;
        volatile boolean playing;
        boolean paused = true;
        Canvas canvas;
        Paint paint;
        private long timeThisFrame;

        public BreakoutView(Context context){
            super(context);
            ourHolder = getHolder();
            paint = new Paint();
        }


        @Override
        public void run() {
            while(playing){
                long startFrameTime = System.currentTimeMillis();
                if(!paused){
                    update();
                }
                draw();
                timeThisFrame = System.currentTimeMillis()-startFrameTime;
                if(timeThisFrame >= 1){
                    fps = 1000 / timeThisFrame;
                }
            }
        }

        public void update(){

        }

        public void draw(){
            if(ourHolder.getSurface().isValid()){
                canvas = ourHolder.lockCanvas();
                canvas.drawColor(Color.argb(255,  26, 128, 182));
                paint.setColor(Color.argb(255, 255, 255, 255));
                //paddle
                //ball
                //brick
                //HUD
                ourHolder.unlockCanvasAndPost(canvas);
            }
        }

        public void pause(){
            playing = false;
            try{
                gameThread.join();
            }catch (InterruptedException e){
                Log.e("Error", "Join Thread");
            }
        }


        public void resume(){
            playing = true;
            gameThread = new Thread(this);
            gameThread.start();
        }

        @Override
        public boolean onTouchEvent(MotionEvent motionEvent) {

            switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {

                // Player has touched the screen
                case MotionEvent.ACTION_DOWN:

                    paused = false;

                    if(motionEvent.getX() > screenX / 2){
                        paddle.setMovementState(paddle.RIGHT);
                    }
                    else{
                        paddle.setMovementState(paddle.LEFT);
                    }

                    break;

                // Player has removed finger from screen
                case MotionEvent.ACTION_UP:

                    paddle.setMovementState(paddle.STOPPED);
                    break;
            }
            return true;
        }
    }
    @Override
    protected void onPause(){
        super.onPause();
        breakoutView.pause();
}
